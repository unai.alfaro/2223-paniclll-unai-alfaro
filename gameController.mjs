import { getHeroes } from "./gameService.mjs";

function chooseRandomHero(data) {
  let hero = null;

  do {
    hero = data[Math.floor(Math.random() * data.length)];
  } while (hero.name === "Junkpile");

  let heroHitPoints = hero.powerstats.strength * 10;

  if (heroHitPoints > 666) {
    heroHitPoints = 666;
  }

  hero.powerstats.hitpoints = heroHitPoints;

  return hero;
}

function chooseVIllageHero(data) {
  let village;
  data.map((item) => {
    if (item.name == "Junkpile") {
      village = item;
    }
  });

  let villageHitPoints = village.powerstats.strength * 10;

  if (villageHitPoints > 666) {
    villageHitPoints = 666;
  }

  village.powerstats.hitpoints = villageHitPoints;

  return village;
}

const randomCube = (times, min, max) => {
  let cumulative = 0;
  for (let i = 1; i <= times; i++) {
    cumulative =
    cumulative + Math.floor(Math.random() * (max - min + 1) + min);
  }
  return cumulative;
};

function combat(attacker, advocate) {
  let cube100 = randomCube(1,1,100);

  if (cube100 <= attacker.powerstats.combat) {
    console.log(
      `${attacker.name} obtiene un ${cube100} y ataca con exito`
    );
    //ATAQUE CON ÉXITO
    let cube20 = randomCube(1,1,20);
    if (cube20 == 1 || cube20 == 2) {
      //PIFIA
      let fumbleDamage;
      if (cube20 == 1) {
        //PIFIA 1
        let cube3 = randomCube(1,1,3);
        fumbleDamage = Math.ceil(attacker.powerstats.speed / cube3);
      } else {
        //PIFIA 2
        let cubeTotal = randomCube(4,1,3);
        fumbleDamage = Math.ceil(attacker.powerstats.speed / cubeTotal);
      }
      attacker.powerstats.hitpoints = Math.ceil(
        attacker.powerstats.hitpoints - fumbleDamage
      );
      console.log(
        `FAIL!! ${attacker.name} obtiene un ${cube20} y se clava el arma en su pierna izda. Recibe un daño de ${fumbleDamage} puntos`
      );
    } else if (cube20 >= 3 && cube20 <= 17) {
      //daño normal
      let normalDamage = Math.ceil(
        ((attacker.powerstats.power + attacker.powerstats.strength) * cube20) /
          100
      );
      advocate.powerstats.hitpoints = Math.ceil(
        advocate.powerstats.hitpoints - normalDamage
      );
      console.log(
        `${attacker.name} obtiene un ${cube20} y ejerce un daño de ${normalDamage} puntos`
      );
    } else {
      //ATAQUE CRÍTICO
      let normalDamage;
      let criticalDamage;

      if (cube20 === 18) {
        let cube3 = randomCube(1,1,3);
        criticalDamage = Math.ceil(
          ((attacker.powerstats.intelligence * attacker.powerstats.durability) /
            100) *
            cube3
        );
      } else if (cube20 === 19) {
        let cubeTotal = randomCube(2,1,3);
        criticalDamage = Math.ceil(
          ((attacker.powerstats.intelligence * attacker.powerstats.durability) /
            100) *
            cubeTotal
        );
      } else {
        let cubeTotal = randomCube(3,1,5);
        criticalDamage = Math.ceil(
          ((attacker.powerstats.intelligence * attacker.powerstats.durability) /
            100) *
            cubeTotal
        );
      }
      normalDamage = Math.ceil(
        ((attacker.powerstats.power + attacker.powerstats.strength) * cube20) /
          100
      );
      let totalCriticalDamage = normalDamage + criticalDamage;
      advocate.powerstats.hitpoints = Math.ceil(
        advocate.powerstats.hitpoints - totalCriticalDamage
      );
      console.log(
        `CITRICAL HIT!! ${attacker.name} obtiene un ${cube20} y ejerce un daño de ${totalCriticalDamage} puntos`
      );
    }
  } else {
    //ATAQUE FALLIDO
    console.log(
      `${attacker.name} obtiene un ${cube100} y ha fallado`
    );
  }
  let result = [attacker, advocate];
  return result;
}

function heroStats(hero) {
  console.log("-------------------------------------");
  console.log(`Nombre: ${hero.name}`);
  console.log(`Intelligence: ${hero.powerstats.intelligence}`);
  console.log(`Strength: ${hero.powerstats.strength}`);
  console.log(`Speed: ${hero.powerstats.speed}`);
  console.log(`Durability: ${hero.powerstats.durability}`);
  console.log(`Power: ${hero.powerstats.power}`);
  console.log(`Combat: ${hero.powerstats.combat}`);
  console.log(`HP: ${hero.powerstats.hitpoints}`);
  console.log("-------------------------------------");
}

function chooseFirstTurn (hero, village){
  let villageIntCom =
    village.powerstats.intelligence + village.powerstats.combat;
  let heroIntCom = hero.powerstats.intelligence + hero.powerstats.combat;

  if (villageIntCom > heroIntCom) {
    village.combatTurn = true;
    hero.combatTurn = false;
    console.log("-------------------------------------");
    console.log(`El primer asalto es para ${village.name}`);
    console.log("-------------------------------------");
  } else {
    village.combatTurn = false;
    hero.combatTurn = true;
    console.log("-------------------------------------");
    console.log(`El primer asalto es para ${hero.name}`);
    console.log("-------------------------------------");
  }
}

function inializateCombat(hero, village) {

  chooseFirstTurn(hero, village)

  let roundCount = 0;

  do {
    roundCount = roundCount + 1;

    console.log(`Comienza el asalto ${roundCount}`);

    let heroVillage = null;

    if (village.combatTurn === true) {
      console.log("-------------------------------------");
      console.log(`El asalto es para: ${village.name}`);
      console.log("-------------------------------------");

      heroVillage = combat(village, hero);

      heroStats(heroVillage[0]);
      heroStats(heroVillage[1]);

      village = heroVillage[0];
      hero = heroVillage[1];

      village.combatTurn = false;
      hero.combatTurn = true;
    } else {
      console.log("-------------------------------------");
      console.log(`El asalto es para: ${hero.name}`);
      console.log("-------------------------------------");

      heroVillage = combat(hero, village);
      heroStats(heroVillage[0]);
      heroStats(heroVillage[1]);

      hero = heroVillage[0];
      village = heroVillage[1];

      village.combatTurn = true;
      hero.combatTurn = false;
    }

    if (hero.powerstats.hitpoints <= 0) {
      console.log(`${hero.name} ha sido derrotado.`);
    }

    if (village.powerstats.hitpoints <= 0) {
      console.log(`${village.name} ha sido derrotado.`);
    }
  } while (hero.powerstats.hitpoints > 0 && village.powerstats.hitpoints > 0);
}

const getAllHeroes = async () => {
  try {
    const data = await getHeroes();

    let hero = chooseRandomHero(data);
    let village = chooseVIllageHero(data);

    inializateCombat(hero, village);
  } catch (error) {
    console.log(error);
  }
};

export function gameInizialitation() {
  getAllHeroes();
}
