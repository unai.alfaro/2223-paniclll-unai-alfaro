import fetch from "node-fetch";

export const getHeroes = async () =>{
    return fetch('https://cdn.jsdelivr.net/gh/akabab/superhero-api@0.3.0/api/all.json')
    .then(response => response.json())
}



